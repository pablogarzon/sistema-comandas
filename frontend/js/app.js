var app = angular.module('app', []);
var url = "http://localhost:3000/";

app.controller("comandaController", function ($scope, $http) {
  $scope.productos = [];
  $scope.seleccionados = [];
  //alerta
  $scope.clase = "default";
  $scope.mostrarAlerta = false;
  $scope.mensaje = "";

  cargarProductos();

  function cargarProductos() {
    $http({
      method: "GET",
      url: url + "productos",
    }).then(function (res) {
      $scope.productos = res.data;
    });
  }

  $scope.agregar = function (prodIndex) {
    var producto = $scope.productos[prodIndex];

    var index = 0;
    for (var i = 0; i < $scope.seleccionados.length; i++) {
      if($scope.seleccionados[i].producto.nombre === producto.nombre){
        if($scope.seleccionados[i].cantidad < 10)
          $scope.seleccionados[i].cantidad = $scope.seleccionados[i].cantidad+1;
        index = 1;
      }
    }

    if(index === 0){
      $scope.seleccionados.push({producto: producto, cantidad:1});
    }
  }

  $scope.guardar = function () {
    var comanda = {
      productos: $scope.seleccionados,
      estado: "Nuevo",
      fechaYHora: new Date()
    };

    $http({
      method: "POST",
      url: url + "comanda",
      data: JSON.stringify({comanda})
    }).then(function (res) {
      $scope.clase = "success";
      $scope.mensaje = res.data.result;
      $scope.mostrarAlerta = true;
      $scope.cancelar();
    }, function (err) {
      $scope.clase = "danger";
      $scope.mostrarAlerta = true;
      $scope.mensaje = "error al guardar la comanda.";
    });
  }

  $scope.cancelar = function () {
    $scope.seleccionados = [];
  }
})
.controller("detalleComandaController", function ($scope, $http) {
  $scope.comandas = [];
  $scope.mostrarAlerta = false;
  $scope.mensaje = "";

  cargarComandas();

  function cargarComandas() {
    $http({
      method: "GET",
      url: url + "comandas",
    }).then(function (res) {
      console.log(res.data);
      $scope.comandas = res.data;
    });
  }

  $scope.actualizarEstado = function (index) {
    var comanda = $scope.comandas[index];
    $http({
      method: "PUT",
      url: url + "comanda",
      data: JSON.stringify({id: comanda._id, estado: comanda.estado})
    }).then(function (res) {
      comanda.estado = res.data.estado;
    }, function (err) {
      $scope.mensaje = err.data;
      $scope.mostrarAlerta = true;
    });
  }

  $scope.verComanda = function (estado) {
    if(estado === 'Entregado')
      return { display: 'none' };
    else
      return { display: 'block-inline' };
  }

  $scope.obtenerClase = function (estado) {
    var clase = "";
    switch (estado) {
      case "Nuevo":
        clase = "danger";
        break;
      case "En preparación":
        clase = "warning";
        break;
      case "Terminado":
        clase = "success";
        break;
      case "Entregado":
        clase = "default";
        break;
      default:
        clase = "default";
        alert("estado no válido");
    }
    return clase;
  }
})
