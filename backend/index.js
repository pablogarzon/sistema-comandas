var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//bodyParser
app.use(bodyParser.json());

//mongo connection
mongoose.connect('mongodb://localhost:27017/sistemaComandas');
mongoose.connection.on('error', function () {
  console.log("error en la conexion a la bd")
}).once('open', function () {
  console.log("conexion a la bd ok")
});

//models
var Productos = require('./models/productos');
var Comanda = require('./models/comanda');

//-----------------rutas-----------------
//permitir cors
app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

//obtener todos los productos
app.get('/productos', function (req, res) {
  console.log("solicitud para obtener todos los productos");
  Productos.find({}, function (err, productos) {
      if (err) res.send(err);
      else {
        res.status(200).json(productos);
      }
  });
});

//guardar comanda
app.post('/comanda', function (req, res) {
  console.log("solicitud para guardar comanda");
  var comanda = req.body.comanda;
  var pedido = new Comanda({
    productos: comanda.productos,
    estado: comanda.estado,
    fechaYHora: comanda.fechaYHora
  });
  pedido.save(function (err) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({ result : "comanda guardada" });
    }
  })
});

//actualizar comanda
app.put('/comanda', function (req, res) {
  console.log("solicitud para actualizar una comanda");
  var idComanda = req.body.id;
  var estado = "";
  console.log(req.body.estado);
  switch (req.body.estado) {
    case "Nuevo":
      estado = "En preparación";
      break;
    case "En preparación":
      estado = "Terminado";
      break;
    case "Terminado":
      estado = "Entregado";
      break;
    default:
      estado = "Estado no válido";
  }
  if(estado != "Estado no válido"){
    Comanda.update(
      {_id: idComanda},
      {estado: estado},
      function (err) {
        if (err) res.status(404).send(err);
        else {
          res.json({
            result: 'el estado de la comanda ha sido cambiado',
            estado: estado
          });
        }
      });
  } else {
    res.status(400).send(estado);
  }
});

//ver el detalle de las comandas
app.get('/comandas', function (req, res) {
  console.log("solicitud para ver comandas");
  Comanda.find({estado: {$ne: "Entregado"}}, function (err, comandas) {
    if (err) res.status(404).send(err);
    else res.status(200).json(comandas);
  });
});

app.listen(3000, function() {
  console.log("programa corriendo en el puerto 3000")
});
