const mongoose = require('mongoose');

let productosSchema = mongoose.Schema({
    nombre: String,
    tipo: Number
});

module.exports = mongoose.model('productos', productosSchema);
