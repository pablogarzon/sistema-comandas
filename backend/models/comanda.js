var mongoose = require('mongoose');

var comandaSchema = mongoose.Schema({
    productos: [],
    estado: String,
    fechaYHora: Date
});

module.exports = mongoose.model('comanda', comandaSchema);
